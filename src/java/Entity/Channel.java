/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entity;

import DB.ContentDB;
import DB.ChannelDB;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 *
 * @author Usuario
 */
public class Channel {
    
    private int idChannel,parentChannel;
    private String title, language, picture;
    
    public Channel() {
    }
        
    public Channel(int idChannel,String title, String language, String picture, int parentChannel ) {
        this.title = title;
        this.language = language;
        this.picture = picture;
        this.idChannel = idChannel;
        this.parentChannel = parentChannel;
    }

    public int getParentChannel() {
        return parentChannel;
    }

    public void setParentChannel(int parentChannel) {
        this.parentChannel = parentChannel;
    }

    public int getIdChannel() {
        return idChannel;
    }

    public void setIdChannel(int idChannel) {
        this.idChannel = idChannel;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public String getPicture() {
        return picture;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }
    /**
     * Method for get an ArrayList with all the content of the channel
     * @return Returns an ArrayList with all the content of the channel
     * @throws SQLException 
     */
    public ArrayList<Content> getContent() throws SQLException{
        ContentDB contentdb = new ContentDB();
        ArrayList<Content> content = new ArrayList<Content>();
        content = contentdb.showContentOfChannel(idChannel);
        return content;
    }
    /**
     * Method for get an ArrayList with all the subchannels of the channel
     * @return Returns an ArrayList with all the subchannels of the channel
     * @throws SQLException 
     */
    public ArrayList<Channel> getSubChannels() throws SQLException{
        ChannelDB channeldb = new ChannelDB();
        ArrayList<Channel> subChannel = new ArrayList<Channel>();
        for (int i = 0; i < subChannel.size(); i++) {
            System.out.println(subChannel.get(i).title);
        }
        subChannel = channeldb.showSubChannels(idChannel);
        return subChannel;
    }
    
    
}
