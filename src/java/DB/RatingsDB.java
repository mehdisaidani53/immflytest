/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DB;


import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

/**
 *
 * @author Usuario
 */
public class RatingsDB extends DBConnector {
    /**
     * Method for implement all the parent methods.
     * @throws SQLException 
     */
    public RatingsDB() throws SQLException {
        super();
    }
    /**
     * Method for return in ArrayList the rating of all the channels
     * @return Returns an ArrayList with the rating of every channel.
     * @throws SQLException 
     */
    public ArrayList<String[]> showRatingsOfAllChannels() throws SQLException {
        ArrayList<String[]> rating = new ArrayList<String[]>();
        try {
            String consulta = "SELECT c.title, rc.rating FROM rating_channels rc JOIN channels c ON rc.idChannel = c.idChannel ORDER BY rc.rating DESC";

            Statement statement = this.conn.createStatement();
            ResultSet result = statement.executeQuery(consulta);
            while (result.next()) {
                String[] ratings ={result.getString("c.title"),result.getString("rc.rating")};
                System.out.println("ratingbd: "+ratings);
                rating.add(ratings);
            }
            statement.close();
            result.close();
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return rating;
    }
}
