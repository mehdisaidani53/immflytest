
package DB;

import Entity.Channel;
import Entity.Content;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;


public class ContentDB extends DBConnector {
    /**
     * Method for implement all the parent methods.
     * @throws SQLException 
     */
    public ContentDB() throws SQLException {
        super();
    }
    /**
     * Mehtod for show all the content of one channel.
     * @param idChannel Parameter for use in the query for show only the content of one channel
     * @return Returns an Arraylist with the content of one channel.
     * @throws SQLException 
     */
    public ArrayList<Content> showContentOfChannel(int idChannel) throws SQLException {
        ArrayList<Content> puntosDeVenta = new ArrayList<Content>();
        try {
            String consulta = "SELECT cha.title,ch.*,c.pathContent,m.movie,a.author,d.director,g.genre FROM content c "
                    + "JOIN movie m ON c.idContent = m.idContent "
                    + "JOIN author a ON c.idContent = a.idContent "
                    + "JOIN director d ON c.idContent = d.idContent "
                    + "JOIN genre g ON c.idContent = g.idContent "
                    + "JOIN content_ch ch ON c.idContent = ch.idContent "
                    + "JOIN channels cha ON ch.idChannel = cha.idChannel "
                    + "WHERE cha.idChannel = '" + idChannel + "' AND c.idContent = (SELECT idContent FROM content_ch   WHERE idChannel='" + idChannel + "')";
            System.out.println(consulta);
            Statement statement = this.conn.createStatement();
            ResultSet result = statement.executeQuery(consulta);
            while (result.next()) {
                Content content = new Content(
                        result.getString("c.pathContent"),
                        result.getString("m.movie"),
                        result.getString("d.director"),
                        result.getString("g.genre"),
                        result.getString("a.author"));
                puntosDeVenta.add(content);
            }
            statement.close();
            result.close();
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return puntosDeVenta;
    }

}
