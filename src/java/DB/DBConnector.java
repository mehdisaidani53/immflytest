package DB;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;


public class DBConnector {
    
    protected Connection conn;
    // MySQL Library
    public String driver = "com.mysql.cj.jdbc.Driver";

    // Database name
    public String database = "immfly_db";

    // Host
    public String hostname = "localhost";

    // Port
    public String port = "3306";

    // Path of my database (desactivate the SSLL usage with "?useSSL=false")
    public String url = "jdbc:mysql://" + hostname + ":" + port + "/" + database + "?useSSL=false&serverTimezone=UTC";

    // username
    public String username = "root";

    // User pass
    public String password = "root";

    
    /**
     * Method for connect to the database.
     * @throws SQLException 
     */
    public DBConnector() throws SQLException {
        try {
            Class.forName(driver);

        } catch (Exception e) {
            e.printStackTrace();
        }
        conn = DriverManager.getConnection(url, username, password);
    }
    /**
     * Method for close the connection to the database.
     * @throws SQLException 
     */
    public void close() throws SQLException{
        conn.close();
    }
    
    
}
