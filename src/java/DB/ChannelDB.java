package DB;

import Entity.Channel;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;


public class ChannelDB extends DBConnector {

    public ChannelDB() throws SQLException {
        super();
    }

    /**
     * Method for select save all the channels in ArrayList for after show them
     * in the front end.
     *
     * @return Returns an ArrayList with all the channels
     * @throws SQLException
     */
    public ArrayList<Channel> showChannels() throws SQLException {
        ArrayList<Channel> channels = new ArrayList<Channel>();
        try {
            String consulta = "SELECT * FROM channels";

            Statement statement = this.conn.createStatement();
            ResultSet result = statement.executeQuery(consulta);
            while (result.next()) {
                Channel channel = new Channel(result.getInt("idChannel"), result.getString("title"), result.getString("language"), result.getString("picture"),result.getInt("parentChannel"));
                if (channel.getParentChannel() == channel.getIdChannel()) {
                    channels.add(channel);
                }
            }
            statement.close();
            result.close();
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return channels;
    }
    /**
     * Method for select  all the channels in ArrayList for after show them.
     * @return Returns an ArrayList with all the channels
     * @throws SQLException
     */
    /**
     * Method for select  all the subchannels in ArrayList for after show them.
     * @param idChannel Parameter requested for select only the subchannels referenced to the parent channel
     * @return Returns an ArrayList with all the channels
     * @throws SQLException 
     */
    public ArrayList<Channel> showSubChannels(int idChannel) throws SQLException {
        ArrayList<Channel> subchannels = new ArrayList<Channel>();
        try {
            String consulta = "SELECT * FROM channels WHERE parentChannel LIKE '"+idChannel+"'";

            Statement statement = this.conn.createStatement();
            ResultSet result = statement.executeQuery(consulta);
            while (result.next()) {
                Channel channel = new Channel(result.getInt("idChannel"), result.getString("title"), result.getString("language"), result.getString("picture"),result.getInt("parentChannel"));
                if (channel.getParentChannel()!=channel.getIdChannel()) {
                    subchannels.add(channel);
                }
                
            }
            statement.close();
            result.close();
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return subchannels;
    }

    

}
