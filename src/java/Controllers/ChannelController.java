package Controllers;

import DB.ChannelDB;
import DB.RatingsDB;
import Entity.Channel;
import com.opencsv.CSVWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


public class ChannelController extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        //Variable for receive the parameter with the function we want to use
        String function ="hola";
        function = request.getParameter("fc");
        System.out.println("El valor de function es:"+function);
        switch (function){
            /**
             * Case showChannels: return to showChannels.jsp(Its only example) the ArrayList with all the channels for show them
             */
            case "showChannels":
                try{
                    ChannelDB channeldb = new ChannelDB();
                    ArrayList<Channel> channels = new ArrayList<Channel>();
                    channels = channeldb.showChannels();
                    request.setAttribute("channels", channels);
                    String channelsPage = "showChannels.jsp";
                    RequestDispatcher rd = request.getRequestDispatcher(channelsPage);
                    rd.forward(request, response);
                    channeldb.close();
                    
                }catch(SQLException e){
                    e.printStackTrace();
                }
                break;
            /**
             * Case downloadRatingsOfChannels: Downloads the ratings of all the channels in the Documents folder.
             */
            case "downloadRatingsOfChannels":
                try{
                    RatingsDB ratingsdb = new RatingsDB();
                    ArrayList<String[]> ratings = new ArrayList<String[]>();
                    ratings = ratingsdb.showRatingsOfAllChannels();
                    String User = System.getProperty("user.home");
                    String pathFileCSV=User+"\\Documents\\rating.csv";
                    CSVWriter writer = new CSVWriter(new FileWriter(pathFileCSV));
                    String[] header = { "Title of Channel", "Rating of Channel"}; 
                    writer.writeNext(header); 
                    writer.writeAll(ratings);
                    writer.close();
                    String indexPage = "index.jsp";
                    RequestDispatcher rd = request.getRequestDispatcher(indexPage);
                    rd.forward(request, response);
                    ratingsdb.close();
                    
                }catch(SQLException | ServletException e){
                    e.printStackTrace();
                }
                break;
                
            default:
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
