CREATE DATABASE  IF NOT EXISTS `immfly_db` /*!40100 DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci */ /*!80016 DEFAULT ENCRYPTION='N' */;
USE `immfly_db`;
-- MySQL dump 10.13  Distrib 8.0.18, for Win64 (x86_64)
--
-- Host: localhost    Database: immfly_db
-- ------------------------------------------------------
-- Server version	8.0.18

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `author`
--

DROP TABLE IF EXISTS `author`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `author` (
  `idAuthor` int(11) NOT NULL AUTO_INCREMENT,
  `author` varchar(45) NOT NULL,
  `idContent` int(11) NOT NULL,
  PRIMARY KEY (`idAuthor`),
  KEY `FK_AUTHOR_CONTENT_idx` (`idContent`),
  CONSTRAINT `FK_AUTHOR_CONTENT` FOREIGN KEY (`idContent`) REFERENCES `content` (`idContent`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `author`
--

LOCK TABLES `author` WRITE;
/*!40000 ALTER TABLE `author` DISABLE KEYS */;
INSERT INTO `author` VALUES (1,'test1',1),(2,'test2',2),(3,'test3',3),(4,'test4',4);
/*!40000 ALTER TABLE `author` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `channels`
--

DROP TABLE IF EXISTS `channels`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `channels` (
  `idChannel` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(45) NOT NULL,
  `language` varchar(45) DEFAULT NULL,
  `picture` varchar(45) DEFAULT NULL,
  `parentChannel` int(11) NOT NULL,
  PRIMARY KEY (`idChannel`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `channels`
--

LOCK TABLES `channels` WRITE;
/*!40000 ALTER TABLE `channels` DISABLE KEYS */;
INSERT INTO `channels` VALUES (1,'La que se avecina','Spanish','images/lqsv.png',3),(2,'The Blacklist','English','images/thblacklist.png',4),(3,'Atresplayer','Spanish','iamges/test.png',3),(4,'Englishplayer','English','iamges/test2.png',4),(5,'Mentalist','English','iamges/mentalist.png',5),(6,'Got Talent','Spanish','images/gottalent.png',3);
/*!40000 ALTER TABLE `channels` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `content`
--

DROP TABLE IF EXISTS `content`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `content` (
  `idContent` int(11) NOT NULL AUTO_INCREMENT,
  `pathContent` varchar(150) NOT NULL,
  PRIMARY KEY (`idContent`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `content`
--

LOCK TABLES `content` WRITE;
/*!40000 ALTER TABLE `content` DISABLE KEYS */;
INSERT INTO `content` VALUES (1,'videos/test1.mp3'),(2,'videos/test2.mp3'),(3,'/pdfs/test3.pdf'),(4,'pdfs/test4.pdf');
/*!40000 ALTER TABLE `content` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `content_ch`
--

DROP TABLE IF EXISTS `content_ch`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `content_ch` (
  `idContent` int(11) NOT NULL,
  `idChannel` int(11) NOT NULL,
  PRIMARY KEY (`idContent`,`idChannel`),
  KEY `FK_CH_CONTENT_idx` (`idChannel`),
  CONSTRAINT `FK_CH_CONTENT` FOREIGN KEY (`idChannel`) REFERENCES `channels` (`idChannel`),
  CONSTRAINT `FK_CONTENT_CH` FOREIGN KEY (`idContent`) REFERENCES `content` (`idContent`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `content_ch`
--

LOCK TABLES `content_ch` WRITE;
/*!40000 ALTER TABLE `content_ch` DISABLE KEYS */;
INSERT INTO `content_ch` VALUES (1,1),(2,2),(3,5),(4,6);
/*!40000 ALTER TABLE `content_ch` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `director`
--

DROP TABLE IF EXISTS `director`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `director` (
  `idDirector` int(11) NOT NULL AUTO_INCREMENT,
  `director` varchar(45) NOT NULL,
  `idContent` int(11) NOT NULL,
  PRIMARY KEY (`idDirector`),
  KEY `FK_DIRECTOR_CONTENT_idx` (`idContent`),
  CONSTRAINT `FK_DIRECTOR_CONTENT` FOREIGN KEY (`idContent`) REFERENCES `content` (`idContent`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `director`
--

LOCK TABLES `director` WRITE;
/*!40000 ALTER TABLE `director` DISABLE KEYS */;
INSERT INTO `director` VALUES (1,'director1',1),(2,'director2',2),(3,'director3',3),(4,'director4',4);
/*!40000 ALTER TABLE `director` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `genre`
--

DROP TABLE IF EXISTS `genre`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `genre` (
  `idGenre` int(11) NOT NULL AUTO_INCREMENT,
  `genre` varchar(45) NOT NULL,
  `idContent` int(11) NOT NULL,
  PRIMARY KEY (`idGenre`),
  KEY `FK_GENRE_CONTENT_idx` (`idContent`),
  CONSTRAINT `FK_GENRE_CONTENT` FOREIGN KEY (`idContent`) REFERENCES `content` (`idContent`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `genre`
--

LOCK TABLES `genre` WRITE;
/*!40000 ALTER TABLE `genre` DISABLE KEYS */;
INSERT INTO `genre` VALUES (1,'genre1',1),(2,'genre2',2),(3,'genre3',3),(4,'genre4',4);
/*!40000 ALTER TABLE `genre` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `movie`
--

DROP TABLE IF EXISTS `movie`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `movie` (
  `idMovie` int(11) NOT NULL AUTO_INCREMENT,
  `movie` varchar(45) NOT NULL,
  `idContent` int(11) NOT NULL,
  PRIMARY KEY (`idMovie`),
  KEY `FK_MOVIE_CONTENT_idx` (`idContent`),
  CONSTRAINT `FK_MOVIE_CONTENT` FOREIGN KEY (`idContent`) REFERENCES `content` (`idContent`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `movie`
--

LOCK TABLES `movie` WRITE;
/*!40000 ALTER TABLE `movie` DISABLE KEYS */;
INSERT INTO `movie` VALUES (1,'movie1',1),(2,'movie2',2),(3,'movie3',3),(4,'movie4',4);
/*!40000 ALTER TABLE `movie` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `rating_channels`
--

DROP TABLE IF EXISTS `rating_channels`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `rating_channels` (
  `idRatingChannels` int(11) NOT NULL AUTO_INCREMENT,
  `rating` varchar(45) NOT NULL,
  `idChannel` int(11) NOT NULL,
  PRIMARY KEY (`idRatingChannels`),
  KEY `FK_RATING_CH_idx` (`idChannel`),
  CONSTRAINT `FK_RATING_CH` FOREIGN KEY (`idChannel`) REFERENCES `channels` (`idChannel`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `rating_channels`
--

LOCK TABLES `rating_channels` WRITE;
/*!40000 ALTER TABLE `rating_channels` DISABLE KEYS */;
INSERT INTO `rating_channels` VALUES (1,'7.5',1),(2,'7.5',2),(5,'5',5),(6,'4',6);
/*!40000 ALTER TABLE `rating_channels` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `rating_content`
--

DROP TABLE IF EXISTS `rating_content`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `rating_content` (
  `idRatingContent` int(11) NOT NULL AUTO_INCREMENT,
  `rating` varchar(45) NOT NULL,
  `idContent` int(11) NOT NULL,
  PRIMARY KEY (`idRatingContent`),
  KEY `FK_RATING_CONTENT_idx` (`idContent`),
  CONSTRAINT `FK_RATING_CONTENT` FOREIGN KEY (`idContent`) REFERENCES `content` (`idContent`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `rating_content`
--

LOCK TABLES `rating_content` WRITE;
/*!40000 ALTER TABLE `rating_content` DISABLE KEYS */;
INSERT INTO `rating_content` VALUES (1,'10',1),(2,'5',2),(3,'5',3),(4,'4',4);
/*!40000 ALTER TABLE `rating_content` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-12-04 19:22:19
