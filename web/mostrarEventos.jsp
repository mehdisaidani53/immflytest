<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib prefix = "c" uri = "http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix = "fmt" uri = "http://java.sun.com/jsp/jstl/fmt" %>
<%@page import="java.util.ArrayList"%>
<%@page import="Entity.Channel"%>
<%@page import="DB.ChannelDB"%>


<%-- 
    This part of Java is for Call the method showChannels in ChannelDB and show them in the HTML part.
--%>
<%

    ChannelDB pdv = new ChannelDB();
    ArrayList<Channel> channels = new ArrayList<Channel>();
    channels = pdv.showChannels();

    request.setAttribute("channels", channels);

%>

<%-- 
    This part of HTML is for show every channel with his respective subchannel, the content and also de metadata.
--%>
<div>
    <h1 style="margin-bottom:0;">Channels </h1>
    <c:forEach items="${channels}" var="channel">
        <div>
            <figure>
                <figcaption>
                    <h2><c:out value="${channel.getTitle()}" /></h2>
                    <div><p>Language: <c:out value="${channel.getLanguage()}"/></p></div>
                    <div><p>PathOfPicture: <c:out value="${channel.getPicture()}" /></p></div>
                    <div style="margin-bottom:20px" >
                        <p style="margin-bottom:0;"> </p>
                        <c:forEach items="${channel.getContent()}" var="content">
                            <div><p>Path of content: <c:out value="${content.getPathContent()}"/></p></div>
                            <div><p>Director <c:out value="${content.getDirector()}" /></p></div>
                            <div><p>Genre <c:out value="${content.getGenre()}" /></p></div>
                            <div><p>Author <c:out value="${content.getAuthor()}" /></p></div>
                        </c:forEach>
                    </div>
                    <div style="margin-bottom:20px; margin-left: 12%" >
                        <%-- SubChannels--%>
                        <c:forEach items="${channel.getSubChannels()}" var="subchannel">
                            <h2><c:out value="${subchannel.getTitle()}"/></h2>
                            <div><p>Language: <c:out value="${subchannel.getLanguage()}" /></p></div>
                            <div><p>PathOfPicture: <c:out value="${subchannel.getPicture()}" /></p></div>
                            <div style="margin-bottom:20px; margin-left: 12%" >
                                <p style="margin-bottom:0;"> </p>
                                <c:forEach items="${subchannel.getContent()}" var="contentOfSubChannel">
                                    <div><p>Path of content: <c:out value="${contentOfSubChannel.getPathContent()}"/></p></div>
                                    <div><p>Director <c:out value="${contentOfSubChannel.getDirector()}" /></p></div>
                                    <div><p>Genre <c:out value="${contentOfSubChannel.getGenre()}" /></p></div>
                                    <div><p>Author <c:out value="${contentOfSubChannel.getAuthor()}" /></p></div>
                                </c:forEach>
                            </div>
                        </c:forEach>
                    </div>
                </figcaption>
            </figure></div>
        </c:forEach>
</div>
